# Tanuki Scroller

## Installation

* Clone repository
* Go to `chrome://extensions`
* Toggle `Developer Mode` to on
* Click `Load unpacked`
* Select repository directory

## Usage

Visit any GitLab page and a `Scroll Down` and `Scroll Up` links will render.

Clicking the links will navigate a user to the bottom and top of the page immediately without needing to scroll manually.

The intention here is to save time to get to the latest conversations in GitLab especially on older or chattier issues.
