//chrome.storage.local.get(console.log)

var scrollerItem = document.getElementsByClassName('issuable-sidebar-header');
//alert(scrollerItem.length);
//prompt("How many hours to snooze?", "24");
//var btn = document.createElement('a');
//btn.className = 'tanuki-scroller-scroll-to-bottom';
//btn.innerText = 'Scroll';
//btn.addEventListener('click', scroll, false);    
//scrollerItem.appendChild(btn);
//document.body.appendChild(btn);

const element = document.getElementById("content-body");

for (var i = 0; i < scrollerItem.length; i++ ) {
    var div = document.createElement('div');
    div.className = 'tanuki-scroller-scroll-to-bottom';

    var btnDown = document.createElement('a');
    btnDown.className = 'scroll-to-bottom';
    btnDown.innerText = 'Scroll Down';
    btnDown.addEventListener('click', scrollDown, false);    

    div.append(btnDown);

    var btnUp = document.createElement('a');
    btnUp.className = 'scroll-to-top';
    btnUp.innerText = 'Scroll Up';
    btnUp.addEventListener('click', scrollUp, false);    

    div.append(btnUp);
    scrollerItem[i].append(div);
}

function scrollDown(ev) {
    element.scrollIntoView(false);
    
    //let pageBottom = document.querySelector('#atwho-ground-note-body');
    //pageBottom.scrollIntoView();
    
    //let scrollToBottom = document.querySelector('#scroll-to-bottom');
    //let pageBottom = document.querySelector('#page-bottom');
    //scrollToBottom.addEventListener('click', function () {
    //    pageBottom.scrollIntoView();
    //})
}

function scrollUp(ev) {
    element.scrollIntoView(true);
    //let pageTop = document.querySelector('#content-body');
    //pageTop.scrollIntoView();
}
